<?php

use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/third-party-example', function (Request $request) {
    // query params:
    //   - (string) regicare_client_code: unique RegiCare Client code (e.g. DEMO)
    //   - (string) regicare_oauth_url: URL to use for oauth authentication
    // . - (string) regicare_oncomplete_url: url to go back to on completion in the third party app
    //
    // For example: https://regicare-oauth-example-app.localhost/third-party-example?regicare_client_code=DEMO&regicare_oauth_url=https%3A%2F%2Fdemo.regicare.nl%2Fapi%2Fv2%2Foauth&regicare_oncomplete_url=https%3A%2F%2Fwww.adsysco.nl

    // Lookup table of all the known RegiCare clients.
    $clientInfo = [
        'DEMO' => [
            'name' => 'RegiDemo',
            'api' => 'https://demo.regicare.nl/api/v2',
        ],
        'EXAMPLE' => [
            'name' => 'RegiExample',
            'api' => 'https://example.regicare.nl/api/v2',
        ],
        'ENV' => [
            'name' => 'RegiEnv',
            'api' => env('API_URL')
        ]
    ];

    // Save the oncomplete_url to the session for later use.
    $request->session()->put('regicare_oncomplete_url', $request->get('regicare_oncomplete_url'));

    // Save the client info for later use
    $request->session()->put('regicare_client_info', $clientInfo[$request->get('regicare_client_code')]);

    // Start the oauth flow.
    $request->session()->put('state', $state = Str::random(40));
    $query = http_build_query([
        'client_id' => env('OAUTH_CLIENT_ID'),
        'redirect_uri' => env('APP_URL') . '/callback',
        'response_type' => 'code',
        'scope' => 'profile',
        'state' => $state,
    ]);

    return redirect($request->get('regicare_oauth_url') . '/authorize?'.$query);
});

Route::get('/start-oauth', function (Request $request) {
    $request->session()->put('state', $state = Str::random(40));

    $query = http_build_query([
        'client_id' => env('OAUTH_CLIENT_ID'),
        'redirect_uri' => env('APP_URL') . '/callback',
        'response_type' => 'code',
        'scope' => 'profile',
        'state' => $state,
    ]);

    return redirect(env('OAUTH_URL') . '/authorize?'.$query);
});

Route::get('/callback', function (Request $request) {
    $state = $request->session()->pull('state');

    throw_unless(
        strlen($state) > 0 && $state === $request->state,
        InvalidArgumentException::class
    );

    $response = Http::asForm()->post(env('OAUTH_URL') .'/token', [
        'grant_type' => 'authorization_code',
        'client_id' => env('OAUTH_CLIENT_ID'),
        'client_secret' => env('OAUTH_CLIENT_SECRET'),
        'redirect_uri' => env('APP_URL') . '/callback',
        'code' => $request->code,
    ]);

    $request->session()->put('accessToken', $response->json('access_token'));
    $request->session()->put('tokenResponse', $response->json());

    return redirect('/userinfo');

    return $response->json();
});


Route::get('userinfo', function (Request $request) {
    $accessToken = $request->session()->get('accessToken');

    $response = Http::withHeaders([
        'Accept' => 'application/json',
        'Authorization' => 'Bearer '.$accessToken,
    ])->get(env('API_URL') . '/user');

    // $response->json contains the user object.
    // It is an array with:
    //   'id' => 1234 (userID in the RegiCare instance (not unique across multiple RegiCare client instances!))
    //   'email' => user@example.com (email adress)
    //   'nickname' => 'Jane' (Firstname)
    //   'name' => 'Jane Doe (Fullname)

    return view('user', [
        'user' => $response->json(),
        'onComplete' => $request->session()->get('regicare_oncomplete_url')
    ]);
});

Route::get('list-interests', function (Request $request) {
    $accessToken = $request->session()->get('accessToken');

    $response = Http::withHeaders([
        'Accept' => 'application/json',
        'Authorization' => 'Bearer '.$accessToken,
    ])->post(env('API_URL') . '/users/120/interests', ['key' => 'diet.lesssalt']);

    $response = Http::withHeaders([
        'Accept' => 'application/json',
        'Authorization' => 'Bearer '.$accessToken,
    ])->post(env('API_URL') . '/users/120/interests', ['key' => 'diet.moresalt']);

    $response = Http::withHeaders([
        'Accept' => 'application/json',
        'Authorization' => 'Bearer '.$accessToken,
    ])->delete(env('API_URL') . '/users/120/interests/diet.moresalt');

    $response = Http::withHeaders([
        'Accept' => 'application/json',
        'Authorization' => 'Bearer '.$accessToken,
    ])->get(env('API_URL') . '/users/120/interests');

    return $response->json();
});

Route::get('legacy', function (Request $request) {
    $accessToken = $request->session()->get('accessToken');
    abort_if(!$accessToken, 403, 'You need a valid access token to access the user info');

    $response = Http::withHeaders([
        'Accept' => 'application/json',
        'Authorization' => 'Bearer '.$accessToken,
    ])->post(env('API_LEGACY_URL') . '/profielGegevens', [env('API_LEGACY_APIKEY')]);

    return $response->json();
});
