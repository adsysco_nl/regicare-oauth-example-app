<x-layout>
    <header>
        <h1>Welcome</h1>
    </header>
    <main>
        <p>Welcome to the RegiCare OAuth Example app!</p>

        <div>
            <h2>Basic login with RegiCare</h2>
            <p>This starts the OAuth flow based on the endpoint in the .env file</p>
            <a href="/start-oauth">Login with RegiCare</a>
        </div>

        <div>
            <h2>Third party app example</h2>
            <p>This uses query parameters to auto-kickoff login against the appropriate RegiCare Client</p>
            <a href="/third-party-example?regicare_client_code=DEMO&regicare_oauth_url=https%3A%2F%2Fdemo.regicare.nl%2Fapi%2Fv2%2Foauth&regicare_oncomplete_url=https%3A%2F%2Fwww.adsysco.nl">
                Login with RegiCare
            </a>
        </div>
    </main>
</x-layout>
