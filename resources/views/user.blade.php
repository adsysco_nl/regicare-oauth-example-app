
<x-layout>
    <header>
        <h1>User info</h1>
    </header>
    <main>
        @if(data_get($user, 'id'))

            <dl>
                <dt>ID</dt>
                <dd>{{ data_get($user, 'id') }}</dd>

                <dt>Email</dt>
                <dd>{{ data_get($user, 'email') }}</dd>

                <dt>Nickname</dt>
                <dd>{{ data_get($user, 'nickname') }}</dd>

                <dt>Name</dt>
                <dd>{{ data_get($user, 'name') }}</dd>
            </dl>

            @if($onComplete)
                <a href="{{ $onComplete }}">
                    Go back to My RegiCare
                </a>
            @endif

        @else
            <h1>You need to authenticate first!</h1>
            <a href="/start-oauth">Login with RegiCare</a>
        @endif
    </main>
</x-layout>
