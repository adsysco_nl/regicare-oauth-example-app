# RegiCare OAuth demo application

## Getting started
1. Clone this repository
2. Run `composer install`
3. Create an oAuth client at the oAuth provider
4. Copy `.env.example` to `.env`
5. Fill out the required `.env` variables

## Trying it out
Visit `http://path-to-this-app.com/redirect`.
This should start the oAuth flow:
 - asking for your username + password
 - requesting permission for the app to user the API on your behalve
 - redirecting you back to your app

 After a succesfull attempt, you can go to
 `/userinfo` to display some basic user information that is gathered from the API.
