# Apply env values
INPUT=$(echo $1| base64 -d)

# Stop the program immediatly when an error occurs
set -eo pipefail

# Create env file name
ENV_FILE=".env.$VIRTUAL_HOST"

# Create env file
printf "$INPUT" | envsubst > $ENV_FILE
