composer dump-autoload
php artisan view:clear
php artisan route:clear
php artisan config:clear
php artisan optimize --env=$VIRTUAL_HOST
rm /tmp/wsdl-*