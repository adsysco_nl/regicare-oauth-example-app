php artisan crm:sync --fresh --forceAll --env=$VIRTUAL_HOST
# Retry if failed
if [ $? == 1 ]; then
    printf "\n--- Retrying\n"
    php artisan crm:sync --fresh --forceAll --env=$VIRTUAL_HOST
fi